<?php include 'header.html'; ?>

	<div class="large-8 columns">
		<h2>Staff Login</h2>
		
		<form
			action="scripts/userLogin.php"
			method="POST">

			<fieldset>
				<legend>Log In</legend>
				<h4>Log in with your staff account</h4>

				<div class="row">
					<label>Email address</label>
					<input type="text" name="loginEmail" placeholder="example@example.com" />
				</div>
				<div class="row">
					<label>Password</label>
					<input type="password" name="loginPassword" placeholder="Password" />
					<input type='text' name="as_staff" value='true' style='display:none'/>
				</div>
				<div class="row">
					<div class="large-4 columns">
						<input type="submit" class="button expand" value="Log in" />
					</div>
				</div>
			</fieldset>
		</form>
	</div>


<?php include 'footer.html'; ?>
