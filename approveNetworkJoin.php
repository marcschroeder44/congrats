<?php include 'header.html';
	session_start();
	include 'scripts/dbConnect.php';
?>

<div class="large-8 columns">

	<h2>Network Joins waiting for approval</h2>

	<?php

	$staff = $_SESSION["isStaff"];
	$email = $_SESSION["userEmail"];

	$qstring = sprintf("SELECT DISTINCT userEmail, groupName, affiliationApproved, groupID, administratorUser FROM `congratsAffiliations` NATURAL JOIN `congratsGroups` WHERE affiliationApproved=0 AND administratorUser='%s' LIMIT 0, 30 ",$email);
	


	$result = mysqli_query($db,$qstring);

	if(mysqli_num_rows($result) == 0){
		echo '<p><strong>No joins to approve at this time.</strong></p>';
	} else {
		echo '<form action="scripts/approveJoin.php" method="POST">';

		while($row = mysqli_fetch_assoc($result)){
			
			printf('<p><strong>%s - %s</strong>&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" name="%s,%s" value="approve" /> Approve&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" name="%s,%s" value="deny" /> Deny
							</p>',
							$row['userEmail'],
							$row['groupName'],
							$row['userEmail'],
							$row['groupID'],
							$row['userEmail'],
							$row['groupID']);


			// printf('<p><input type="checkbox" name="checked_joins[]" value="%s,%s" />&nbsp;&nbsp;&nbsp;%s - %s</p>',	
			// 		$row["userEmail"],
			// 		$row["groupID"],
			// 		$row["userEmail"],
			// 		$row["groupName"]
			// );
		}

		echo '<input type="submit" class="button" value="Approve/Deny Joins" />';
		echo '</form>';
	}



	?>

</div>

<div class="large-4 columns">
	<a href="congrats.php">Back</a>
</div>

<?php include 'footer.html'; ?>