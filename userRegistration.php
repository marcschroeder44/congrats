<?php include 'header.html'; ?>

	<div class="large-8 columns">

		<!-- PUT MAIN CONTENT IN HERE -->
		<h2>Register for an account</h2>
				
				<form
					action="scripts/addUser.php"
					method="POST">
		
					<fieldset>
						<legend>Register</legend>
		
						<div class="row">
							<label>Email address</label>
							<input type="text" name="loginEmail" placeholder="example@example.com" />
						</div>
						<div class="row">
							<label>Password</label>
							<input type="text" name="loginPassword" placeholder="Password" />
						</div>
						<div class="row">
							<label>Name</label>
							<input type="text" name="loginName" placeholder="Name" />
						<div class="row">
							<div class="large-4 columns">
								<input type="submit" class="button expand" value="Log in" />
							</div>
						</div>
					</fieldset>
				</form>
		
		
	</div>

	<div class="large-4 columns">
		<!-- SIDEBAR CONTENT GOES HERE -->
	</div>


<?php include 'footer.html'; ?>
