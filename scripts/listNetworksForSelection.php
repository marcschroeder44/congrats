<?php
	session_start();
	
	include 'dbConnect.php';

	$email = $_SESSION['userEmail'];

	$sql = sprintf("SELECT * FROM congratsGroups WHERE groupApproved=1 AND congratsGroups.groupID NOT IN (SELECT DISTINCT groupID FROM `congratsAffiliations` NATURAL JOIN `congratsGroups` WHERE userEmail='%s')", $email);

	//$result = mysqli_query($db, "SELECT * FROM congratsGroups");
	$result = mysqli_query($db, $sql);

	if(mysqli_num_rows($result) == 0){
		echo '<p><strong>No networks exist.</strong></p>';
	}
	else{
		while($row = mysqli_fetch_assoc($result)){
			printf('<p><input type="checkbox" name="checked_networks[]" value="%s" />&nbsp;&nbsp;&nbsp;%s
				<span class="label secondary round">%s</span>
				<em>%s</em></p>',
					$row["groupID"],
					$row["groupName"],
					$row["administratorUser"],
					$row["groupDescription"]
			);
		}
	}

?>
