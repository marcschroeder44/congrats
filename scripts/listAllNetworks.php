<?php

	include 'dbConnect.php';

	$result = mysqli_query($db, "SELECT * FROM congratsGroups");

	if(mysqli_num_rows($result) == 0){
		echo '<p><strong>No networks exist.</strong></p>';
	}
	else{
		while($row = mysqli_fetch_assoc($result)){
			//echo $row["groupApproved"]; continue;
			if($row["groupApproved"] == 0){
				printf('<p><span class="round alert label">%s</span> %s : <small><em>Group not yet approved</em></small></p>',
						$row["groupID"],
						$row["groupName"]
				);
			}
			else {
				printf('<p><span class="round secondary label">%s</span> %s</p>',
					$row["groupID"],
					$row["groupName"]
				);
			}
		}
	}

?>
