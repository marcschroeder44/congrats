<?php
	
	include 'dbConnect.php';

	session_start();

	$email = $_POST['loginEmail'];
	$password = $_POST['loginPassword'];
	$asStaff = $_POST['as_staff'];

	$_SESSION['userEmail'] = $email;
	$_SESSION['userPassword'] = $password;
	$_SESSION['isStaff'] = $asStaff;

	echo 'Email = ' . $email . '<br />';
	echo 'Passw = ' . $password . '<br />';
	echo 'Is Staff: ' . $asStaff . '<br />';

	$qstring = sprintf("SELECT * FROM congratsUsers WHERE userEmail='%s'", mysqli_real_escape_string($db, $email));
	$result = mysqli_query($db, $qstring);

	if(mysqli_num_rows($result) == 0){
		echo '<br />No results with that email.<br />';
		exit();
	}

	$row = mysqli_fetch_assoc($result);
	// printf("<li>%s, %s, %s, %s</li>", $row["userEmail"], $row["userPassword"], $row["isStaff"], $row["userName"]);

	if ($row['userPassword'] != $password){
		echo "<br />Incorrect Password<br />";
		exit();
	}

	if ($asStaff == "true" && $row["isStaff"] == 0){
		echo "Deeply sorry, but you cannot login as a staff member because you aren't one. Please use user page.<br/>";
	} else if ($asStaff == "true" && $row["isStaff"] == 1) {
		echo "Logged in as staff.<br/>";
		header('Location: ../congrats.php');
		// header('Location: ../congrats.php?email='.$email."&staff=1");
	} else if ($asStaff == "false" && $row["isStaff"] == 1) {
		// Echo that they should use the staff login page.
		echo "You tried to sign in as a user.<br />But you're actually a staff member.<br />
			 Use the <a href='../staffLogin.php'>staff login</a> page instead.";
	} else {
		echo "Logged in as user.<br/>";
		header('Location: ../congrats.php');
		// header('Location: ../congrats.php?email='.$email."&staff=0");
	}

	echo '</ul>';

?>
