<?php

	include 'dbConnect.php';

	$email = $_SESSION['userEmail'];

	// Function to fix up PHP's messing up POST input containing dots, etc.
	function getRealPOST() {
		$pairs = explode("&", file_get_contents("php://input"));
		$vars = array();
		foreach ($pairs as $pair) {
			$nv = explode("=", $pair);
			$name = urldecode($nv[0]);
			$value = urldecode($nv[1]);
			$vars[$name] = $value;
		}
		return $vars;
	}

	$items = getRealPOST();

	foreach ($items as $emailAndNetwork=>$choice){

		//echo $emailAndNetwork . '=>' . $choice;
	
		$joinInfo = explode(',',$emailAndNetwork);

		if($choice == "approve"){
			$qstring = sprintf("UPDATE congratsAffiliations SET affiliationApproved=1 WHERE userEmail='%s' AND groupID='%d'",$joinInfo[0],$joinInfo[1]);
			//echo $qstring;
			if(($result = mysqli_query($db, $qstring)) === FALSE){
					echo '<strong>Problem approving network join </strong>';
			}
		} else if ($choice == "deny"){
			$qstring = sprintf("DELETE FROM congratsAffiliations WHERE userEmail='%s' AND groupID='%d'",$joinInfo[0],$joinInfo[1]);
			//echo $qstring;
			if(($result = mysqli_query($db, $qstring)) === FALSE){
					echo '<strong>Problem deleting network join </strong>';
			}
		} else {
			continue;
		}
	}

	header("Location: ../congrats.php");

?>