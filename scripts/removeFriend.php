<?php
	session_start();
	include 'dbConnect.php';


	// Function to fix up PHP's messing up POST input containing dots, etc.
	function getRealPOST() {
		$pairs = explode("&", $_SERVER['QUERY_STRING']);
		$vars = array();
		foreach ($pairs as $pair) {
			$nv = explode("=", $pair);
			$name = urldecode($nv[0]);
			$value = urldecode($nv[1]);
			$vars[$name] = $value;
		}
		return $vars;
	}



	$email1 = $_GET["e1"];
	$email2 = $_GET["e2"];

	$qstring = sprintf("DELETE FROM congratsFriends WHERE
						(email='%s' AND friendsWith='%s') OR
						(email='%s' AND friendsWith='%s')",
						$email1, $email2,
						$email2, $email1
	);
	if(($result = mysqli_query($db, $qstring)) === FALSE){
		echo '<strong>Problem removing friendship between ' . $email1 . ' and ' . $email2 . '</strong>';
	}

	header("Location: ../congrats.php");

?>
