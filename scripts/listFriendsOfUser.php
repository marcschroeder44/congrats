<?php 

	session_start();

	include 'dbConnect.php';

	$email = $_SESSION['userEmail'];
	$qstring = sprintf("SELECT DISTINCT j1.userEmail AS email1, j1.userName AS name1,
			j2.userEmail AS email2, j2.userName AS name2, friendshipApproved
			FROM
			(SELECT userEmail, userName 
			 	FROM congratsUsers NATURAL JOIN congratsFriends 
				WHERE congratsUsers.userEmail = congratsFriends.email) AS j1,
			(SELECT userEmail, userName 
			 	FROM congratsUsers NATURAL JOIN congratsFriends 
				WHERE congratsUsers.userEmail = congratsFriends.friendsWith) AS j2,
			congratsFriends
			WHERE congratsFriends.email = j1.userEmail
			AND congratsFriends.friendsWith = j2.userEmail
			AND (j1.userEmail='%s' OR j2.userEmail='%s')",
				$email,
				$email );
	$result = mysqli_query($db, $qstring);

	if(mysqli_num_rows($result) == 0){
		echo "<p class='warning'><strong>It appears you have no friends :(</strong></p>";
	}
	else{
		echo "<h3>Your friends</h3>";
		while($row = mysqli_fetch_assoc($result)){
			// Check which email is the current signed-in user
			if($row["email1"] == $email){
				// Print the other person's name
				printf('<p>%s ', $row["name2"]);
			}
			else {
				// Print the first person's name
				printf('<p>%s ', $row["name1"]);
			}

			if($row["friendshipApproved"] == 0){
				printf('<small><span class="round alert label">
						Friendship awaiting approval
						</span></small>');
			}
			else {
				// Print a link to remove friend
				printf('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="scripts/removeFriend.php?e1=%s&e2=%s">Remove</a>',
						$row["email1"],
						$row["email2"]
				);
			}

			// Close the paragraph tag
			printf('</p>');
		}
		
	}

	?>
