<?php 
	session_start();
	include 'dbConnect.php';

	$email = $_SESSION['userEmail'];

	$requestedFriends = $_POST['friend_requests'];

	if($requestedFriends){
		foreach($requestedFriends as $requested){
			// Add one row for me, them
			$qstring = sprintf("INSERT INTO congratsFriends VALUES ('%s', '%s', 0)",
					$email,
					$requested );
			if(($result = mysqli_query($db, $qstring)) === FALSE){
				echo '<strong>Problem requesting friendship for pair ' . $email . ', ' . $requested . '</strong>';
			}
		}
	}
	header("Location: ../congrats.php");
?>
