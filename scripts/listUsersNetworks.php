<?php

	session_start();

	include 'dbConnect.php';

	$email = $_SESSION['userEmail'];

	$qstring = sprintf("SELECT * FROM congratsAffiliations NATURAL JOIN congratsGroups WHERE userEmail='%s'", $email);
	$result = mysqli_query($db, $qstring);

	if(mysqli_num_rows($result) == 0){
		echo "<p class='warning'><strong>You haven't joined any networks yet!</strong></p>";
	}
	else{
		echo "<h3>Your Networks</h3>";
		while($row = mysqli_fetch_assoc($result)){
			if ($row["affiliationApproved"] == 1){
				printf('<p><span class="round secondary label">%s</span> %s',
					$row["groupID"],
					$row["groupName"]);
			} else {
				printf('<p><span class="round secondary label">%s</span> %s 
						<small><span class="round alert label">Network join awaiting approval</span></small>',
					$row["groupID"],
					$row["groupName"]);
			}

			if($email != $row["administratorUser"]){
				// Print a link to remove friend
				printf('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="scripts/removeFromNetwork.php?emailIn=%s&networkIDIn=%s">Remove</a>',
						$email,
						$row["groupID"]);
			}

			printf('</p>');

		}
	}

?>
