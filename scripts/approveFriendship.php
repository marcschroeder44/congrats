<?php
	session_start();
	include 'dbConnect.php';

	$email = $_SESSION['userEmail'];

	// Function to fix up PHP's messing up POST input containing dots, etc.
	function getRealPOST() {
		$pairs = explode("&", file_get_contents("php://input"));
		$vars = array();
		foreach ($pairs as $pair) {
			$nv = explode("=", $pair);
			$name = urldecode($nv[0]);
			$value = urldecode($nv[1]);
			$vars[$name] = $value;
		}
		return $vars;
	}


	if($_POST){

		$items = getRealPOST();

		foreach($items as $requester=>$choice){

			echo $requester . '=>' . $choice;

			if($choice === "approve"){
				$qstring = sprintf("UPDATE congratsFriends SET friendshipApproved=1 WHERE email='%s'
									AND friendsWith='%s'",
									$requester,
									$email
				);
				if(($result = mysqli_query($db, $qstring)) === FALSE){
					echo '<strong>Problem approving friendship with ' . $requester . '</strong>';
				}
			}
			else if($choice === "deny"){
				$qstring = sprintf("DELETE FROM congratsFriends
									WHERE email='%s' AND friendsWith='%s'",
									$requester,
									$email
				);
				if(($result = mysqli_query($db, $qstring)) === FALSE){
					echo '<strong>Problem deleting request from ' . $requester . '</strong>';
				}
			}
			else {
				continue;
			}
		}
	}

	header("Location: ../congrats.php");

?>
