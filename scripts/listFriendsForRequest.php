<?php
	session_start();

	include 'dbConnect.php';

	$email = $_SESSION['userEmail'];

	$sql = sprintf("SELECT userEmail, userName FROM congratsUsers
					WHERE userEmail NOT IN
					(SELECT friendsWith AS userEmail
					 	FROM congratsFriends
						WHERE email='%s' )
					AND userEmail NOT IN
					(SELECT email AS userEmail
					 	FROM congratsFriends
						WHERE friendsWith='%s' )
					AND userEmail!='%s'",
			$email,
			$email,
		    $email );

	$result = mysqli_query($db, $sql);

	if(mysqli_num_rows($result) == 0){
		echo '<p>Da bomb. You\'re friends with <strong>EVERYONE</strong></p>';
	}
	else {
		while($row = mysqli_fetch_assoc($result)){
			printf('<p><input type="checkbox" name="friend_requests[]" value="%s" />&nbsp;&nbsp;&nbsp;%s
					<span class="label secondary round">%s</span></p>',
					$row["userEmail"],
					$row["userName"],
					$row["userEmail"] );
		}
	}
?>
