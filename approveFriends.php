<?php include 'header.html'; 
	session_start();
	include 'scripts/dbConnect.php';
?>

	<div class="large-8 columns">

		<!-- PUT MAIN CONTENT IN HERE -->
		<h2>Approve Friend Requests</h2>
		<?php 
			$email = $_SESSION['userEmail'];

			// Grab rows where the 'requested' friend is in friendsWith
			// and the friendship is not yet approved
			$qstring = sprintf("SELECT email, userName FROM congratsFriends NATURAL JOIN congratsUsers
								WHERE friendsWith='%s' AND friendshipApproved='0'
								AND email=congratsUsers.userEmail", $email);
			$result = mysqli_query($db, $qstring);

			if(mysqli_num_rows($result) == 0){
				echo "<p>No friendship requests to approve at this time.</p>";
			}
			else {

				echo '<form action="scripts/approveFriendship.php" method="POST">';

				while($row = mysqli_fetch_assoc($result)){
					printf('<p><strong>%s</strong>&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" name="%s" value="approve" /> Approve&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" name="%s" value="deny" /> Deny
							</p>',
							$row["userName"],
							$row["email"],
							$row["email"]
					);
				}

				echo '<input type="submit" class="button" value="Approve/Deny Friendship" />';
				echo '</form>';
			}
		?>


	</div>

	<div class="large-4 columns">
		<!-- SIDEBAR CONTENT GOES HERE -->
  		<a href="congrats.php">Return</a>
	</div>

<?php //session_destroy(); ?>
<?php include 'footer.html'; ?>
