<?php include 'header.html'; 
	session_start();
?>

	<div class="large-8 columns">

		<!-- PUT MAIN CONTENT IN HERE -->

		<h2>Submit a new network application</h2>

		<form 
			action="scripts/addNetworkApp.php"
			method="POST">

			<fieldset>
				<div class="row">
					<label>Network Name</label>
					<input type="text" name="networkName"/>
				</div>
				<div class="row">
					<label>Discription</label>
					<input type="text" name="networkDescription"/>
				</div>
				<div class="row">
					<div>
						<input type="submit" class="button expand" value="submit"/>
					</div>
				</div>
			</fieldset>
	</form>
		


	</div>

	<div class="large-4 columns">
		<!-- SIDEBAR CONTENT GOES HERE -->
		<p><a href="congrats.php">Return</a></p>
	</div>

	</p>

<?php include 'footer.html'; ?>
