<?php include 'header.html'; 
	session_start();
?>

	<div class="large-8 columns">

		<!-- PUT MAIN CONTENT IN HERE -->
		<h2>Welcome!</h2>
		<?php 
			$email = $_SESSION['userEmail'];
			$staff = $_SESSION['isStaff'];

			if ($staff == 'true'){
				echo "Staff member " . $email;	
			} else {
				echo "User " . $email;
			}
			
			echo '<hr />';

		?>
		
		<?php

			if ($staff == 'true'){
				echo '<h3>List of all networks</h3>';
				include 'scripts/listAllNetworks.php';
				echo '<hr />';
				include 'scripts/listUsersNetworks.php';
			}
			else {
				include 'scripts/listUsersNetworks.php';
			}
			echo '<hr />';

			include 'scripts/listFriendsOfUser.php';

		?>


	</div>

	<div class="large-4 columns">
		<!-- SIDEBAR CONTENT GOES HERE -->
		<p align="left">
			<a href="addFriends.php" class="button expand">Find/Add Friends</a>
	  		<a href="selectNetworks.php"  class="button expand">Browse/Join Networks</a>
	  		<a href="applyForNewNetwork.php" class="button expand">Create New Network</a>
			<a href="approveFriends.php" class="button expand">Approve/Deny Friend Requests</a>
			<a href="approveNetworkJoin.php" class="button expand">Approve/Deny Network Joins</a>
	
			<?php
			if ($staff == 'true'){
				echo '<a href="approveNetworks.php" class="button expand">Approve Networks</a>';
				echo '<a href="listUsers.php" class="button expand">See All Users</a>';
				
			}
			?>
		</p>
	</div>

<?php include 'footer.html'; ?>
