<?php include 'header.html';
	session_start();
	include 'scripts/dbConnect.php';
?>

<div class="large-8 columns">

	<h2>Networks waiting for approval</h2>

	<?php
	$staff = $_SESSION["isStaff"];

	if ($staff != 'true'){
		echo "You are not a staff member, and cannot approve networks.<br />
				Please return to the <a href='congrats.php'>home page</a>";
	}
	else {
		$sql = sprintf("SELECT groupID, groupName FROM congratsGroups WHERE groupApproved='0'");

		$result = mysqli_query($db, $sql);

		if(mysqli_num_rows($result) == 0){
			echo '<p><strong>No networks to approve at this time.</strong></p>';
		}
		else {
			echo '<form action="scripts/approveNetworks.php" method="POST">';

			while($row = mysqli_fetch_assoc($result)){
				printf('<p><input type="checkbox" name="checked_networks[]" value="%s" />&nbsp;&nbsp;&nbsp;%s</p>',
						$row["groupID"],
						$row["groupName"]
				);
			}

			echo '<input type="submit" class="button" value="Approve Networks" />';
			echo '</form>';
		}
	}
	?>

</div>

<div class="large-4 columns">
	<a href="congrats.php">Back</a>
</div>

<?php include 'footer.html'; ?>

