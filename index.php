<?php include 'header.html'; ?>

	<div class="large-8 columns">
		<h2>Log In</h2>
		
		<form
			action="scripts/userLogin.php"
			method="POST">

			<fieldset>
				<legend>Log In</legend>
				<h4>Log in with your Congrats account</h4>

				<div class="row">
					<label>Email address</label>
					<input type="text" name="loginEmail" placeholder="example@example.com" />
				</div>
				<div class="row">
					<label>Password</label>
					<input type="password" name="loginPassword" placeholder="Password" />
					<input type='text' name="as_staff" value='false' style='display:none'/>
				</div>
				<div class="row">
					<div class="large-4 columns">
						<input type="submit" class="button expand" value="Log in" />
					</div>
				</div>
			</fieldset>
		</form>
	</div>

	<div class="large-4 columns">
		<p><a href="userRegistration.php"><strong>Register</strong></a></p>
		<p><a href="staffLogin.php">Staff Login</a></p>
	</div>

	

<?php include 'footer.html'; ?>
