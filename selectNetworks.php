<?php include 'header.html'; ?>

<?php session_start(); ?>


	<div class="large-8 columns">

		<!-- PUT MAIN CONTENT IN HERE -->


		<h2>Select networks to join</h2>
		<p class="subheader">Check which networks you would like to join. You do not have to join any networks.</p>

		<form action="scripts/addNetworks.php"
			  method="POST">
		<?php 
		include 'scripts/listNetworksForSelection.php'; ?>
		<?php 
			$email = $_SESSION['userEmail'];
			printf('<input type="text" value="%s" name="userEmail" style="display:none"/>', $email);
		?>

		<input type="submit" class="button" value="Join Networks" />

		</form>


	</div>

	<div class="large-4 columns">
		<!-- SIDEBAR CONTENT GOES HERE -->
		<a href="congrats.php">Return</a>
	</div>


<?php include 'footer.html'; ?>
