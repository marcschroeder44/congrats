<?php include 'header.html'; ?>

<?php session_start(); ?>


	<div class="large-8 columns">

		<!-- PUT MAIN CONTENT IN HERE -->


		<h2>Add Friends</h2>
		<p class="subheader">Checking a name here will send a friend request to that person.
		They will then have the opportunity to accept or reject the friend request.</p>

		<form action="scripts/requestFriends.php"
			  method="POST">
		<?php 
			include 'scripts/listFriendsForRequest.php';
		?>

		<input type="submit" class="button" value="Send Friend Requests" />

		</form>


	</div>

	<div class="large-4 columns">
		<!-- SIDEBAR CONTENT GOES HERE -->
		<p><a href="congrats.php">Go back</a></p>
	</div>


<?php include 'footer.html'; ?>
